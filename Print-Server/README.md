# Print-Server 🖨️

## Einleitung

In dieser Anleitung zeige ich euch, wie man einen Printserver einrichtet.

## Installation des Druckdienstes

Als Erstes muss ein Windows Server installiert werden. Die Installation ist selbsterklärend.

Danach installiere den Druckerdienst.

Klicke dafür auf "Add roles and features" und folge den Installationsanweisungen.

![img1](./images/img1.png)

Installiere die Rolle "Print and Document Services".

![img2](./images/img2.png)

Nach der Installation beginnen wir mit der Konfiguration.

Gehe über "Tools" zu "Print Management".

![img3](./images/img3.png)

Klicke mit der rechten Maustaste und füge den Drucker hinzu.

![img4](./images/img4.png)

![img5](./images/img5.png)

Nach dem Hinzufügen kannst du eine Testseite drucken, um zu prüfen, ob der Drucker korrekt verbunden ist. Bei mir hat es funktioniert.

## Hinzufügen des Druckers auf den Clients

Ich habe den Client benutzt, den ich bereits im Modul 117 vorbereitet habe. Um den Drucker sichtbar zu machen, musst du im Explorer die IP-Adresse mit \\ vorne angeben. Es sollte dann so aussehen wie im Screenshot. Bei mir kam leider immer eine Fehlermeldung, da keine Verbindung hergestellt werden konnte. Es war nicht nur bei mir so, sondern in der ganzen Klasse. Dennoch war der Drucker sichtbar.