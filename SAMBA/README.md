# Samba File Share 📂

[TOC]

Diesmal mussten wir einen Samba Server aufsetzen. Das wird benötigt, um die Dateien extern zu speichern, sodass sie nicht lokal gespeichert werden und auch andere Benutzer im selben Netzwerk darauf zugreifen können – ähnlich wie bei einem NAS.

Für diese Aufgabe habe ich den Ubuntu Desktop Server 22.04.03 verwendet.

## Installation/Konfiguration des Tools über das Terminal

Um dies durchzuführen, habe ich die Anleitung verwendet, die uns zur Verfügung gestellt wurde. Zusätzlich musste ich die Netzwerkkarte vom privaten Netzwerk hinzufügen, damit die Windows VM und der Ubuntu Server sich im Netzwerk sehen können.

Auf dem Desktop angekommen, habe ich die Konsole geöffnet. Hier sind die Schritte, wie ich die Konsole geöffnet habe:

![Registerkarte_Ubuntu](./images/Registerkarte_Ubuntu.png)

![Registerkarte_Terminal](./images/Registerkarte_Terminal.png)

Im Terminal habe ich dann zuerst ein Update durchgeführt mit dem Befehl "sudo apt update".

![Update](./images/Update.png)

Danach habe ich den Samba Dienst installiert mit dem Befehl "sudo apt install samba". Mit dem Befehl "systemctl status smbd --no-pager -l" konnte ich überprüfen, ob der Dienst läuft.

![Samba_Status](./images/Samba_Status.png)

Ich habe dann mit dem Befehl "sudo systemctl enable --now smbd" noch konfiguriert, dass der Dienst immer startet, wenn man die VM startet.

![Samba_auto_start](./images/auto_start.png)

Mit dem Befehl "sudo ufw allow samba" habe ich dann noch der Ubuntu VM erlaubt, auf die Firewall zuzugreifen.

## Benutzer hinzufügen

Ich musste nun nur noch einen Benutzer zum Samba Dienst hinzufügen. Dies kann man mit dem Befehl "sudo usermod -aG sambashare $USER" machen.

## File Share

Ich habe nun über das GUI einen Ordner geteilt. Dafür bin ich links in der Taskleiste auf das Folder Symbol gegangen und dann auf "Persönlicher Ordner" (auf Englisch Home).

![Home_Verzeichnis](./images/Home_Verzeichnis.png)

Mit einem Rechtsklick habe ich dann einen Testordner erstellt und ihn "test" genannt. Dann habe ich mit folgenden Schritten den Ordner geteilt: Rechtsklick auf den Ordner und auf "Eigenschaften" (Properties auf Englisch) - dann auf "Freigabe im lokalen Netzwerk" (Local Network Share auf Englisch) - "Diesen Ordner freigeben". Anschließend muss man noch die Berechtigung für den geteilten Ordner wählen, ich habe sowohl die Berechtigung zum Ändern der Dateien als auch die Gastfreigabe ausgewählt.

![Sharing](./images/Sharing.png)

## Das Testen

Ich habe mich auf dem Windows Client mit dem URL im Explorer \\192.168.164.173\test eingegeben. Dann musste ich das Passwort und den Benutzernamen eingeben. Danach konnte ich darauf zugreifen und die Dateien ändern und erstellen.

![Testerfolgreich](./images/testing.png)

[zurück](https://gitlab.com/Timo_Ranta/m123)