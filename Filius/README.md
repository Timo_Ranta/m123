# Filius Auftrag 🖥️

## Einleitung
Unser Auftrag bestand darin, ein Netzwerk in Filius für eine Firma einzurichten.

## Prozess
Als Erstes habe ich mir die Aufgabe angesehen.

![Bild1](./Images/bild1.png)

Danach habe ich alle Clients, Server, Router und Switches miteinander verbunden, damit sie sich gegenseitig sehen und kommunizieren können.

![Bild2](./Images/bild5.png)

Anschließend habe ich alle Clients entsprechend der Vorgaben konfiguriert. Hier ist ein Bild von der Konfiguration von PC11:

![Bild3](./Images/bild2.png)

Danach habe ich den DHCP-Server konfiguriert. Ich habe ihm einen Adressbereich von 10.0.0.3 bis 10.0.0.10 zugewiesen. Dadurch erhielten alle Clients, die auf DHCP eingestellt waren, automatisch eine IP-Adresse und mussten keine APIPA-Adresse haben.

## Test

Zum Schluss habe ich einen Test durchgeführt, bei dem ich PC51 angepingt habe. Im Screenshot unten kann man sehen, dass dies erfolgreich funktioniert hat. Es gab jedoch ein kleines Problem bei mir, da ich das Gateway für den DHCP-Server falsch eingetragen hatte. Ich hatte das Gateway von RT02 anstelle von RT01 eingetragen. Am Ende habe ich das jedoch korrigiert, und alles hat erfolgreich funktioniert.

![Bild4](./Images/bild4.png)