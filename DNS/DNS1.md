# DNS auf Windows Installieren 🌐

[TOC]

## Einleitung

In dieser Dokumentation werden wir einen Windows DNS Server in der Version 2019 aufsetzen. Der Windows DNS Server war für mich einfacher einzurichten als der DHCP Ubuntu Server.

## DNS Installieren

### Vorbereitung

Um den DNS Server aufzusetzen, benötigt man einen Client mit dem Netzwerk M117 und einem öffentlichen Nat Netzwerk. Ausserdem wird ein Windows Server benötigt (ich habe die Version 2019 verwendet).

### Statische IP-Adresse Vergeben

Nach der Installation des Windows Servers muss diesem eine statische IP-Adresse zugewiesen werden. Meine Konfiguration sieht wie folgt aus:

![IP-config](./images/win2.png)

Die DNS-Adressen (127.0.0.1 und 8.8.8.8) werden aus folgendem Grund zugewiesen:
- 127.0.0.1 ist die localhost IP-Adresse.
- 8.8.8.8 ist die IP-Adresse des Google DNS-Servers.

Speichern Sie diese Einstellungen und starten Sie Ihren Server neu.

### DNS auf Windows-Server Installieren

Öffnen Sie den Server Manager.

Klicken Sie auf "Add roles and features". Hier habe ich eine Tabelle mit Bildern und Beschreibungen erstellt:

| Bilder | Beschreibung |
| - | - |
| ![Installation01](./images/win4.png) | Wählen Sie bei diesem Schritt "Role based or featured based installation" aus.
 ![Installation02](./images/win5.png) | Wählen Sie bei diesem Schritt Ihren Server aus.
 ![Installation03](./images/win6.png) ![Installation04](./images/win7.png) | Wählen Sie "DNS Server Installation" aus und klicken Sie anschliessend auf "Next" und dann auf "Add Features".
 ![Installation06](./images/win9.png) | Setzen Sie ein Häkchen bei "Automatisch neustarten".

## DNS Konfigurieren

### Forward Lookup Zone Einrichten

Jetzt muss eine Forward Lookup Zone eingerichtet werden.

| Bilder | Beschreibung |
| - | - |
 ![Conf1](./images/win12.png) | Klicken Sie auf "Tools" und dann auf "DNS".
![Conf2](./images/win13.png) | Führen Sie einen Rechtsklick auf "Forward Lookup Zone" durch und erstellen Sie eine neue Zone.
 ![Conf3](./images/win14.png) | Wählen Sie "Primary Zone" aus.
 ![Conf4](./images/win15.png) | Geben Sie einen Domainnamen ein (z.B., sota.ch).
 ![conf5](./images/win16.png) | Erstellen Sie eine neue Datei.
 ![Conf6](./images/win17.png) | Erlauben Sie keine dynamischen Updates.

### Reverse Lookup Zone Einrichten

| Bilder | Beschreibung |
| - | - |
| ![Conf7](./images/win23.png) | Führen Sie einen Rechtsklick auf "Reverse Lookup Zone" aus und dann auf "New Zone".
 ![Conf8](./images/win18.png) | Erstellen Sie eine "Primary Zone".
 ![Conf9](./images/win19.png) | Erstellen Sie eine IPv4 Lookup Zone.
 ![Conf10](./images/win20.png) | Geben Sie Ihre Netz-ID ein.
 ![Conf11](./images/win21.png) | Klicken Sie auf "Next".
 ![Conf12](./images/win22.png) | Wählen Sie wieder "Keine dynamischen Updates erlauben" aus.

## Finale Konfigurationen

### Host und Mail Exchanger Hinzufügen

#### Host Hinzufügen

Jetzt müssen noch Hosts hinzugefügt werden.

Führen Sie einen Rechtsklick auf "Forward Lookup Zone" durch und wählen Sie "New Zone" aus.

![host1](./images/host1.png)

Hier können Sie eine Subdomain erstellen. Ich habe meine Subdomain web.sota.ch benannt, aber sie später in server.sota.ch umbenannt.

![host2](./images/host2.png)

Diesen Schritt können Sie für den Client wiederholen. Ich habe den Client client.sota.ch genannt.

#### Mail Exchanger Hinzufügen

Jetzt muss noch ein Mail Exchanger erstellt werden.

Führen Sie einen Rechtsklick auf "Forward Lookup Zone" aus. Wählen Sie dann "New Mail Exchanger" aus.

![mx1](./images/host1.png)

Ich habe den Mail Exchanger mail.sota.ch genannt.

![mx2](./images/host3.png)

Jetzt ist der DNS Server fertig installiert und konfiguriert.

#### DNS Konfiguration auf dem Client

Jetzt muss der Client auch noch entsprechend konfiguriert werden.

Öffnen Sie die Netzwerkeinstellungen und ändern Sie beim Nat Netzwerk die IP-Adresse des DNS auf diese hier:

![Client1](./images/test2.png)

Für den M117 Host-Only Netzwerkadapter ändern Sie die DNS-Einstellungen des DNS Servers wie folgt:

![Client2](./images/test3.png)

## Testing

Ich habe das Ganze in der CMD getestet. Ich habe für den Test diese Befehle verwendet:

```
nslookup server.sota.ch localhost

nslookup mail.sota.ch localhost

nslookup client.sota.ch
```

Das Ergebnis sollte so aussehen:

![DNSTest](./images/Erfolgreicher%20test%20windows%20dns%20client.png)

### Forwarders Konfigurieren

Zum Schluss müssen wir noch den Forwarders konfigurieren.

Mache jetzt einen Rechtsklick auf deinen DNS Server im Server Manager.

Klicke danach auf "Properties".

![forward1](./images/forward1.png)

Klicken Sie dann oben auf "Forwarders" und dann auf "Edit".

Gib hier dann die IP Adresse deines gewünschten Servers ein. Ich habe hier die IP Adresse 8.8.8.8 von Google verwendet, da dieser Server viele Domains gespeichert hat.

Dies ist dazu da, dass Ihr DNS Anfragen, die er nicht bearbeiten kann, weiter an einen anderen Server senden kann.

![forward2](./images/forward2.png)

Ich habe zum Abschluss einen Test an hosttech.ch gemacht. Das Ganze hat super funktioniert. Das war das Ergebnis:

![Test](./images/test%20hosttech.ch.png)

Bei mir hat alles super funktioniert, und ich habe viel Neues dazugelernt.

[zurück](https://gitlab.com/Timo_Ranta/m123)