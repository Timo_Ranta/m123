# M123 Lernjournal 📚

## Einleitung

Herzlich willkommen in meinem Lernjournal! In diesem Repository dokumentiere ich meine Fortschritte und das, was ich im Modul 123 gelernt habe.

## Inhaltsverzeichnis
1. [DHCP-Dienst in Filius](./DHCP/README.md)
2. [Laborübung 1 - DHCP mit Cisco Packet Tracer](./DHCP/01_DHCP.md)
3. [Ubuntu Server Konfiguration](./DHCP/02_DHCP_konfiguration_ubuntuserver.md)
4. [DNS Server Windows Installieren](./DNS/DNS1.md)
5. [Samba Server einrichten](./SAMBA/README.md)
6. [Filius Netzwerk](./Filius/README.md)
7. [Print-Server](./Print-Server/README.md)


## Kontaktinformationen

- **Name:** Timo Ranta
- **E-Mail:** [timo.ranta@edu.tbz.ch](mailto:timo.ranta@edu.tbz.ch)

## Signatur

Unterschrieben von Timo Ranta am 17.12.2023 🖋️