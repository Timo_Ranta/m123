# DHCPServer Ubuntu konfigurieren 🚀

## Inhaltsverzeichnis

- [Einführung](#einführung)
- [Installation Ubuntu Server](#installation-ubuntu-server)
  - [Installation des Servers](#installation-des-servers)
  - [Konfiguration der DHCP-Server](#konfiguration-der-dhcp-server)
  - [Konfiguration der Netzwerkschnittstellen](#konfiguration-der-netzwerkschnittstellen)
  - [Netplan-Konfiguration](#netplan-konfiguration)
- [Fehlerbehebung](#fehlerbehebung)
- [Schlussbemerkung](#schlussbemerkung)

## Einführung

Die Aufgabe bestand darin, einen DHCP-Server unter Ubuntu zu konfigurieren. Ziel war es, den DORA-Prozess reibungslos zu gestalten, damit der Client automatisch eine IP-Adresse aus dem definierten Bereich erhalten kann.

## Installation Ubuntu Server

Als Erstes war die Installation des Betriebssystems erforderlich. Dies erfolgte mithilfe des ISO-Images von Ubuntu Server 22.04.3.

### Installation des Servers

Nach dem Starten des Servers wählte ich "Try or Install Ubuntu Server". Anschliessend folgte ich den Installationsanweisungen und startete den Server neu. Jetzt konnte ich mit den Konfigurationen beginnen.

Um den DHCP-Server zu installieren, verwendete ich den Befehl:

```bash
sudo apt install isc-dhcp-server
```

Nach dem Herunterladen des DHCP-Servers überprüfte ich seinen Status mit:

```bash
sudo systemctl status isc-dhcp-server
```

### Konfiguration der DHCP-Server

Mit dem Befehl `sudo nano /etc/dhcp/dhcpd.conf` öffnete ich die Konfigurationsdatei des DHCP-Servers. Dort habe ich die IP-Adressen und den Bereich konfiguriert.



In dieser Datei entfernte ich alle Hashtags ausser dem ersten unter "A slightly different configuration for an internal subnet". Die Datei sollte dann wie folgt aussehen:

![Bild 1](./images/1.%20konfiguration.png)



### Konfiguration der Netzwerkschnittstellen

Die Schnittstellenkonfigurationsdatei öffnete ich mit dem Befehl:

```bash
sudo nano /etc/default/isc-dhcp-server
```

Dort änderte ich `INTERFACEv4` zu **ens33**, um anzugeben, dass ens33 der Netzwerkadapter für die DHCP-Requests ist.

![Bild 2](./images/2.%20bild%20konfiguration.png)



### Netplan-Konfiguration

Die Netplan-Konfigurationsdatei `00-installer-config.yaml` habe ich mit dem Befehl geöffnet:

```bash
sudo nano /etc/netplan/00-installer-config.yaml
```

Ich konfigurierte die Datei so, dass sie am Ende wie folgt aussah:

![Bild 3](./images/3.%20bild%20konfiguration%20dhcp.png)

Nach diesen Schritten restartete ich die DHCP-Server-Datei:

```bash
sudo systemctl restart isc-dhcp-server
```

Den Status konnte ich mit dem Befehl anzeigen lassen:

```bash
sudo systemctl status isc-dhcp-server
```

## Fehlerbehebung

Trotz der durchgeführten Schritte erhielt ich weiterhin die gleiche Fehlermeldung.

![Fehlermeldung](./images/4.%20bild%20fehler.png)

## Schlussbemerkung

Leider ist mir der Auftrag nicht gelungen, da ich noch nicht ausreichend Erfahrung mit Linux habe. Dennoch habe ich während des Prozesses viel Neues gelernt. 💡

[zurück](https://gitlab.com/Timo_Ranta/m123)