# Laborübung 1 - DHCP mit Cisco Packet Tracer

## 1. Router-Konfiguration auslesen

Wir erhielten den Auftrag, die Konfiguration des Routers auszulesen. Dies wurde gemeinsam durchgeführt. Im Cisco Packet Tracer klickte ich auf den Router und wechselte zur CLI. Dort drückte ich Enter, um auf die Router-Ebene zu gelangen. Die Eingabe sieht so aus:

```bash
R1>
```

Um als Administrator angemeldet zu sein, gab ich den Befehl `enable` ein. Nun bin ich als Administrator angemeldet, und die Eingabe sieht wie folgt aus:

```bash
R1#
```

Anschliessend wollte ich den IP-Pool auslesen, was ich mit dem Befehl durchgeführt habe:

```bash
show ip dhcp pool
```

![Bild 1](./images/DHCP%20show%20ip.png)

Danach habe ich herausgefunden, welche IP-Adresse welcher Client hat und wie lange er diese IP-Adresse behalten kann. Dies habe ich mit dem Befehl `show ip dhcp binding` durchgeführt. Das Ergebnis sieht wie folgt aus:

![Bild 2](./images/DHCP%20ip%20binding.png)

Hier ist zu sehen, dass die Geräte keine Ablaufzeit haben. Das kann daran liegen, dass es sich um Geräte mit festen IP-Adressen handelt, wie z. B. Drucker oder generelle Dienstleistungen. Generelle Dienstleistungen sind Server, auf denen beispielsweise ein DHCP-Server, DNS-Server oder Webserver ausgeführt wird. Abschliessend habe ich den folgenden Befehl eingegeben:

```bash
show running-config
```

Damit wird angezeigt, welche Range von IP-Adressen manuell vergeben werden sollte, da diese im automatischen Bereich liegen.

![Bild 3](./images/running-config.png)

### Fragen:

1. **Für welches Subnetz ist der DHCP-Server aktiv?**

   Der DHCP-Server ist für das /24-Subnetz aktiv.

2. **Welche IP-Adressen sind vergeben und an welche MAC-Adressen?**

   | IP-Adresse      | MAC-Adresse          |
   | --------------- | -------------------- |
   | 192.168.22.34   | 00:E0:8F:4E:65:AA    |
   | 192.168.22.33   | 00:50:0F:4E:1D:82    |
   | 192.168.22.31   | 00:09:7C:A6:4C:E3    |
   | 192.168.22.35   | 00:07:EC:B6:45:34    |
   | 192.168.22.32   | 00:01:63:2C:35:08    |
   | 192.168.22.36   | 00:D0:BC:52:B2:9B    |

3. **In welchem Range vergibt der DHCP-Server IPv4-Adressen?**

   Der DHCP-Server vergibt IPv4-Adressen im Bereich von 192.168.22.1 bis 192.168.22.254.

4. **Was hat die Konfiguration ip dhcp excluded-address zur Folge?**

   Die `ip dhcp excluded-address` Konfiguration legt einen Bereich fest, in dem keine IP-Adressen automatisch vergeben werden dürfen.

5. **Wie viele IPv4-Adressen kann der DHCP-Server dynamisch vergeben?**

   Der DHCP-Server kann 114 IPv4-Adressen dynamisch vergeben.

## 2. DORA - DHCP Lease beobachten

Mit der Anleitung von Aufgabe 2 haben wir den DORA-Prozess simuliert. DORA steht für Discover, Offer, Request, Acknowledge. Zuerst wurde ein Paket per Broadcast an alle Clients gesendet, wobei der Router als einziger aufgrund des DHCP-Dienstes reagierte. Der Router sendete dann das Paket zurück, wieder per Broadcast an alle.

### Fragen:

1. **Welcher OP-Code hat der DHCP-Offer?**

   2

2. **Welcher OP-Code hat der DHCP-Request?**

   1

3. **Welcher OP-Code hat der DHCP-Acknowledge?**

   2

4. **An welche IP-Adresse wird der DHCP-Discover geschickt? Was ist an dieser IP-Adresse speziell?**

   255.255.255.255; Dies ist eine Broadcast-Adresse.

5. **An welche MAC-Adresse wird der DHCP-Discover geschickt? Was ist an dieser MAC-Adresse speziell?**

   FF:FF:FF:FF:FF:FF; Dies ist eine Broadcast-MAC-Adresse.

6. **Warum wird der DHCP-Discover vom Switch auch an PC3 geschickt?**

   Via Broadcast, daher erreicht es alle Geräte im Netzwerk.

7. **Welche IPv4-Adresse wird dem Client zugewiesen?**

   192.168.22.33

8. **Screenshot des DHCP-Discovers PDUs**

   ![Bild 4](./images/discover.png)

9. **Screenshot des DHCP-Offers PDUs**

    ![Bild 5](./images/offer.png)

10. **Screenshot des DHCP-Request PDUs**

    ![Bild 6](./images/request.png)

11. **Screenshot des DHCP-Acknowledge PDUs**

    ![Bild 7](./images/acknogasjkfl.png)

## 3. Netzwerk umkonfigurieren

Der Auftrag

 lautet, dem Server eine statische IP-Adresse über den DHCP-Server zuzuweisen. Ich wusste nicht, wie ich dies über den Router tun konnte, also habe ich es direkt auf dem Server eingerichtet. Ich habe ihm die IP-Adresse 192.168.22.38 zugewiesen.

Wie ich die statische IP-Adresse hinzugefügt habe:

![Bild 8](./images/DHCP%20Server%20ip%20adresse.png)

Die statische Gateway-IP-Adresse hinzugefügt habe:

![Bild 9](./images/gatewa.png)

Ich konnte erfolgreich auf die Website zugreifen:

![Bild 10](./images/website.png)

[zurück](https://gitlab.com/Timo_Ranta/m123)