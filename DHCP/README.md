# DHCP-Dienst in Filius 🌐
## 📚 Inhaltsverzeichnis
- [Einleitung](#einleitung)
- [Hauptteil](#hauptteil)
- [Schluss](#schluss)

## 🚀 Einleitung
In Filius haben wir eine Vorlage mit drei Laptops, einem DHCP-Server und einem Switch erhalten. Uns wurde der Auftrag erteilt, die Geräte über den Switch zu verbinden, um eine Sterntopologie zu erstellen. Dabei mussten wir sicherstellen, dass der DHCP-Server automatisch IP-Adressen an die Laptops verteilt.

## 💻 Hauptteil
Zu Beginn musste ich die Laptops und den DHCP-Server über den Switch verbinden.

![Bild 1](./images/1.png)

Anschliessend aktivierte ich den DHCP-Server, indem ich auf "DHCP-Server einrichten" klickte und dann die Option "DHCP aktivieren" auswählte.

![Bild 2](./images/2.png) 
![Bild 3](./images/3.png)

Nach der Aktivierung des DHCP-Servers fügte ich eine statische IP-Adresse für den Client 3 (den dritten Laptop) hinzu. Dies gewährleistet, dass der Client 3 immer die IP-Adresse 192.168.0.50 erhält.

Um dies zu erreichen, klickte ich im DHCP-Server auf "Einstellungen" und dann auf "Statische IP-Adresse hinzufügen". Dort gab ich die IP-Adresse und die MAC-Adresse des Client 3 ein.

Die Eingaben sahen wie folgt aus:

MAC-Adresse: 49:11:5C:DA:77:23

IP-Adresse: 192.168.0.50

![Bild 4](./images/4.png)

Danach aktivierte ich die DHCP-Einstellung bei allen Laptops. Dies erfolgte auf die gleiche Weise wie beim Aktivieren des DHCP-Servers.

## 🏁 Schluss

Ich habe das Gesamtsystem getestet, indem ich alle Geräte angepingt habe. Im Foto ist zu sehen, wie ich einen Laptop angepingt habe, jedoch habe ich dies auch bei den anderen durchgeführt, ohne jedoch Fotos zu machen.

![Bild Test](./images/Test.png)

[zurück](https://gitlab.com/Timo_Ranta/m123)